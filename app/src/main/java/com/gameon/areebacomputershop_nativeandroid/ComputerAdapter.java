package com.gameon.areebacomputershop_nativeandroid;

import android.content.Context;
import android.media.Image;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

//import com.bumptech.glide.Glide;
//import com.firebase.ui.storage.images.FirebaseImageLoader;
//import com.google.firebase.auth.FirebaseAuth;
//import com.google.firebase.storage.FirebaseStorage;
//import com.google.firebase.storage.StorageReference;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

public class ComputerAdapter extends BaseAdapter {

    Context context;
    ArrayList<Computer> Computers;

    public ComputerAdapter(Context context, ArrayList<Computer> ComputerItems) {
        this.context = context;
        this.Computers = ComputerItems;
    }

    @Override
    public int getCount() {
        return Computers.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = layoutInflater.inflate(R.layout.computer_row, null);

        //Setting the view fields to the list view values
        TextView pcNameView = view.findViewById(R.id.pcName);
        pcNameView.setText(Computers.get(position).getName());


        TextView pcProcessorView = view.findViewById(R.id.pcProcessor);
        pcProcessorView.setText(Computers.get(position).getProcessor() );

        //Setting the image
        //The uuid of the pc is used as a part of image name to link them
        String uuid=Computers.get(position).getUuid();
        ImageView pcImageView= (ImageView) view.findViewById(R.id.pc_image);
         StorageReference ref = FirebaseStorage.getInstance().getReference("appimages-"+uuid);

         Glide.with(context).using(new FirebaseImageLoader()).load(ref).into(pcImageView);




        return view;
    }
}
