package com.gameon.areebacomputershop_nativeandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class WelcomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

    }

    public void moveToGallery(View view){
        Intent i = new Intent(this, HomeActivity.class);
        startActivity(i);
    }

    public void moveToInsert(View view){
        Intent i =new Intent(this,InsertActivity.class);
        startActivity(i);
    }

}
