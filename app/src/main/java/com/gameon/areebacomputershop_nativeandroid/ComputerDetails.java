package com.gameon.areebacomputershop_nativeandroid;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

public class ComputerDetails extends AppCompatActivity {
    private TextView pcNameView,pcBrandView,pcProcessorView,pcUuid,pcMemoryView,pcStorageView;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_computer_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //Receiving the intent from home activity and the extra values of Computer
        Intent i=getIntent();
        Bundle extras=i.getExtras();

        //Setting the Text Views
        pcNameView=(TextView)findViewById(R.id.pcNameDisplay);
        String pcExtraName=(String) extras.get("PC_NAME");
        pcNameView.setText("PC Name: "+pcExtraName);

        pcBrandView=(TextView)findViewById(R.id.pcBrandDisplay);
        String pcExtraBrand=(String) extras.get("PC_BRAND");
        pcBrandView.setText("Pc Brand: "+pcExtraBrand);

        pcProcessorView=(TextView)findViewById(R.id.pcProcessorDisplay);
        String pcExtraProcessor=(String) extras.get("PC_PROCESSOR");
        pcProcessorView.setText("Pc Processor: "+pcExtraProcessor);

        pcMemoryView=(TextView)findViewById(R.id.pcMemoryDisplay);
        Float pcExtraMemory=(Float) extras.get("PC_MEMORY");
        pcMemoryView.setText("Pc Memory: " +pcExtraMemory.toString() );

        pcStorageView=(TextView)findViewById(R.id.pcStorageDisplay);
        Float pcExtraStorage=(Float) extras.get("PC_STORAGE");
        pcStorageView.setText("Pc Storage: "+pcExtraStorage.toString());


        //Setting the image
        ImageView pcImageView= (ImageView) findViewById(R.id.computer_image_display);
        String  uuid= (String)extras.get("PC_UUID");
        Log.d("uuid",uuid);
        StorageReference ref = FirebaseStorage.getInstance().getReference("appimages-"+uuid);

            Glide.with(getApplicationContext()).using(new FirebaseImageLoader()).load(ref).into(pcImageView);


    }

    public void backToGallery(View view) {
        Intent i=new Intent(this,HomeActivity.class);
        startActivity(i);
    }
}
