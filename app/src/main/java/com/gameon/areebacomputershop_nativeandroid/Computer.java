package com.gameon.areebacomputershop_nativeandroid;

public class Computer {

   private String name,brand,processor,uuid;
   private float memory,storage;

    public Computer(String name,String brand,float memory, float storage ,String processor,String uuid ){
      //The UUID is used for unique identifying in the fireBase Structure
        this.name=name;
        this.brand=brand;
        this.memory=memory;
        this.storage=storage;
        this.processor=processor;
        this.uuid= uuid;

    }
    public  Computer(){}

    public String getProcessor() {
        return processor;
    }

    public void setProcessor(String processor) {
        this.processor = processor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public float getMemory() {
        return memory;
    }

    public void setMemory(float memory) {
        this.memory = memory;
    }

    public float getStorage() {
        return storage;
    }

    public void setStorage(float storage) {
        this.storage = storage;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
