package com.gameon.areebacomputershop_nativeandroid;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Debug;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.internal.InternalTokenProvider;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity {
   private ComputerAdapter compAdap;
   private ListView pcListView;
   private ArrayList<Computer> computers;
   private FirebaseDatabase database;
   private DatabaseReference myRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Connecting Fab Button to Insert page
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), InsertActivity.class);
                startActivity(i);
            }
        });

        pcListView = (ListView) findViewById(R.id.computers_list_view);
        computers = new ArrayList<Computer>();
        //Clear the list to prevent duplicates from realtime DB
        computers.clear();

        //Preparing Database retrieval
         database = FirebaseDatabase.getInstance();
         myRef = database.getReference("Computers");



        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot computer_item : dataSnapshot.getChildren()) {
                    Computer retreivedPc= computer_item.getValue(Computer.class);

                    computers.add(retreivedPc);

                }

                //Connecting the Adapter with array list
                compAdap=new ComputerAdapter(getApplicationContext(),computers);
                pcListView.setAdapter(compAdap);


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        pcListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Creating the intent
                Intent i=new Intent(getApplicationContext(),ComputerDetails.class);

                String pcName = computers.get(position).getName();
                i.putExtra("PC_NAME", pcName);
                String pcBrand=computers.get(position).getBrand();
                i.putExtra("PC_BRAND", pcBrand);
                String pcProcessor=computers.get(position).getProcessor();
                i.putExtra("PC_PROCESSOR", pcProcessor);
                String pcUuid=computers.get(position).getUuid();
                i.putExtra("PC_UUID", pcUuid);
                float pcMemory=computers.get(position).getMemory();
                i.putExtra("PC_MEMORY", pcMemory);
                float pcStorage=computers.get(position).getStorage();
                i.putExtra("PC_STORAGE", pcStorage);

                startActivity(i);

            }
        });

    }


}
