package com.gameon.areebacomputershop_nativeandroid;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;


import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.UUID;

import static java.security.AccessController.getContext;

public class InsertActivity extends AppCompatActivity {
    private Button selectImageButt;
    private StorageReference storageRef;
    private static final int PICK_IMAGE_REQUEST = 1;
    private Uri imageUri;
    private ImageView imageView;
    private String uniqueId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        storageRef= FirebaseStorage.getInstance().getReference();
        imageView=(ImageView) findViewById(R.id.computer_image_upload);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imagePicker(v);
            }
        });

        //Creating a unique identifier fot the Pc to insert
         uniqueId = UUID.randomUUID().toString();

    }


    public void submitInsert(View view){
        String pcName= ( (EditText)findViewById(R.id.pcNameEdit)).getText().toString();
        String pcBrand= ( (EditText)findViewById(R.id.pcBrandEdit)).getText().toString();
        String pcProcessor= ( (EditText)findViewById(R.id.pcProcessorEdit)).getText().toString();
        float pcMemory= Float.valueOf(( (EditText)findViewById(R.id.pcMemoryEdit)).getText().toString());
        float pcStorage= Float.valueOf(( (EditText)findViewById(R.id.pcStorageEdit)).getText().toString());

        Computer comp=new Computer(pcName,pcBrand,pcMemory,pcStorage,pcProcessor,uniqueId);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference().child("Computers").child(uniqueId);


        myRef.setValue(comp);
        uploadImage();

    }


    private void imagePicker(View view){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //After the pic has been picked from imagePicker
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data !=null
                && data.getData()!=null){

            imageUri = data.getData();
            Glide.with( InsertActivity.this ).load(imageUri).into(imageView);

        }
    }

    private void uploadImage(){
        if(imageUri != null)
        {
            final ProgressDialog mProgressDialog = new ProgressDialog(InsertActivity.this);
            mProgressDialog.setMessage("Creating Computer...");
            mProgressDialog.show();
            storageRef.child("appimages-"+uniqueId).putFile(imageUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                    //Finished Uploading
                    mProgressDialog.dismiss();
                    Intent i=new Intent(getApplicationContext(),HomeActivity.class);
                    startActivity(i);
                }
            });
        }



    }


}
